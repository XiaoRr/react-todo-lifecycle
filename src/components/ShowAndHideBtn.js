import React, {Component} from 'react';
import TodoList from "./TodoList";

class ShowAndHideBtn extends Component {
  constructor(props){
    super(props);
    this.state = { showHideBtnName : "Show" , showList : true};
    this.switchShowOrHide = this.switchShowOrHide.bind(this);
  }

  switchShowOrHide(){
    if(this.state.showHideBtnName == "Show"){
      this.setState( {showHideBtnName : "Hide"});
      console.log("show");
      return;
    }
    this.setState( {showHideBtnName : "Show"});
    console.log("hide");


  }

  render() {
    return (
      <div>
        <button onClick={this.switchShowOrHide}>{this.state.showHideBtnName}</button>
        <button onClick={() => {document.location.reload()}}>Refresh</button>
        <TodoList showList = {(this.state.showHideBtnName === "Hide")?false:true}/>
      </div>
    );
  }
}

export default ShowAndHideBtn;

