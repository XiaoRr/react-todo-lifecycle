import React, {Component} from 'react';
import './todolist.less';


const ListComponent = (props) => <div>List Title {props.name}</div>;
class TodoList extends Component {

  constructor(props){
    super(props);
    this.state = {
      comps: [],
      count:1
    }
    console.log("Todolist.constructor");
  }

  componentDidMount() {
    console.log("Todolist.componentDidMount");
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    console.log("Todolist.componentDidUpdate");
  }

  componentWillUnmount() {
    console.log("Todolist.componentWillUnmount");
  }

  render() {
    console.log("Todolist.render")
    const { comps } = this.state;
    if(this.props.showList == true)
      return (
        <div>
          <button onClick={() =>{
            this.setState({ comps: comps.concat([this.state.count])});
            this.setState({ count: this.state.count+1})}
          }>
            Add
          </button>
          {
            comps.map(comp => {
                return <ListComponent key={comp} name={comp}/>;
              })
          }
        </div>
      );
    return (
      <div>there should be nothing.</div>
    );
  }
}

export default TodoList;

