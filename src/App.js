import React from 'react';
import './App.less';
import ShowAndHideBtn from "./components/ShowAndHideBtn";

const App = () => {
  return (
    <div className='App'>
      <ShowAndHideBtn />
    </div>
  );
};

export default App;